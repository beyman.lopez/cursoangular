
(async () => {

    //////////// TYPESCRIPT
    // function saludar(nombre: string) {
    //     console.table('Hola, ' + nombre); // Hola Logan
    // }
    // const wolverine = {
    //     nombre: 'Logan'
    // };
    // saludar(wolverine.nombre);
    //////////// TYPESCRIPT
    //////////// SCOPE
    // let mensaje = 'Hola';
    // if (true) {
    //     let mensaje = 'Mundo';
    // }
    // console.log(mensaje);
    //////////// SCOPE
    //////////// TIPADO
    //     let mensaje: string = 'Hola';
    //     let numero: number = 123;
    //     let boleano: boolean = true;
    //     let hoy: Date = new Date();
    //     let cualquiercosa: string | number | Date | boolean; //any
    //     cualquiercosa = mensaje;
    //     cualquiercosa = numero;
    //     cualquiercosa = boleano;
    //     cualquiercosa = hoy;
    //     let spiderman = {
    //         nombre: 'Peter',
    //         edad: 30
    //     }
    //     spiderman = {
    //         nombre: 'Beyman',
    //         edad: 24,
    //     }
    //////////// TIPADO
    //////////// TEMPLATE LITERALES
    //     const nombre = 'Beyman';
    //     const apellido = 'López';
    //     const edad = 24;
    //     const salida = nombre + ' ' + apellido + ' Edad(' + edad + ')'
    //     const salida1 = `${nombre} ${apellido} Edad(${edad})`; // `backtick`
    //     const salida2 = `
    //     ${nombre} 
    //     ${apellido} 
    //     Edad(${edad})`; //salto de lines con espacios extra
    //     const salida3 = `${nombre} 
    // ${apellido} 
    // Edad(${edad})`; //salto de lines sin espacios extra
    //     const salida4 = `${nombre} ${apellido} Edad(${edad + 2})`; // `puedo hacer operaciones, llamar funciones etc dentro de los backtick ${js}`
    //     function getEdad() {
    //         return edad;
    //     }
    //     const salida5 = `${nombre} ${apellido} Edad(${getEdad()})`; // `puedo hacer operaciones, llamar funciones etc dentro de los backtick ${js}`
    //////////// TEMPLATE LITERALES
    //////////// Funciones: Parámetros opcionales, obligatorios y por defecto
    // // orden: 
    // // prmero los Obligatorio
    // // segundo los Opcionales
    // // por último los Por defecto 
    // function activar(
    //     quien: string, //Obligatorio
    //     momento?: string, //Opcional
    //     objeto: string = 'batiseñal' //Por defecto
    // ) {
    //     momento ?
    //         console.log(`${quien} activó la ${objeto} en la ${momento}.`)
    //         :
    //         console.log(`${quien} activó la ${objeto}.`);
    // }
    // activar('Gordon', 'tarde');
    //////////// Funciones: Parámetros opcionales, obligatorios y por defecto
    ////////////  Funciones de Flecha
    // const miFuncionNormal = function (a: string) {
    //     return a.toUpperCase();
    // }
    // const miFuncionFlecha = (a: string) => a.toUpperCase();
    // console.log(miFuncionNormal('beyman'));
    // console.log(miFuncionNormal('beyman'));
    // const sumarN = function (a: number, b: number) {
    //     return a + b;
    // }
    // const sumarN1 = (a: number, b: number) => a + b;
    // console.log(sumarN(1, 2));
    // console.log(sumarN1(1, 2));
    // const hulk = {
    //     nombre: 'Hulk',
    //     smash() {
    //         setTimeout(() => {
    //             console.log(`()=>`);
    //             console.log(`this.nombre ${this.nombre} smash!!`);
    //             console.log(`hulk.nombre ${hulk.nombre} smash!!`);
    //         }, 1000);
    //         setTimeout(function () {
    //             console.log(`function()`);
    //             console.log(`hulk.nombre ${hulk.nombre} smash!!`);
    //             // console.log(`this.nombre ${this.nombre} smash!!`); //ERROR THIS NO ENTRA EN EL SCOPE
    //         }, 1000);
    //     }
    // }
    // hulk.smash();
    ////////////  Funciones de Flecha
    //////////// Desestructruación de Objetos y Arreglos
    // const avenger = {
    //     nombre: 'Steve Rogers',
    //     alias: 'Cap',
    //     poder: 'Droga'
    // }
    // console.log(avenger.nombre);
    // console.log(avenger.alias);
    // console.log(avenger.poder);
    // DESESTRUCTURACION
    // const { nombre, alias, poder } = avenger
    // console.log(nombre);
    // console.log(alias);
    // console.log(poder);
    // DESESTRUCTURACION COMO ARGUMENTO O PARAMETRO
    // const extraer = ({ nombre, alias, poder }: any) => {
    //     console.log(nombre);
    //     console.log(alias);
    //     console.log(poder);
    // }
    // extraer(avenger);
    // // DESESTRUCTURACION DE ARREGLOS
    // const avengers: string[] = ['Beyman', 'Magrego', 'Spiderman', 'Ironman', 'Black widow']
    // console.log(avengers[2]);
    // console.log(avengers[3]);
    // console.log(avengers[4]);
    // const [, , peter, tony, Romanoff] = avengers
    // console.log(peter);
    // console.log(tony);
    // console.log(Romanoff);
    // // DESESTRUCTURACION DE ARREGLOS COMO ARGUMENTO O PARAMETRO
    // const extraerArr = ([, , peter, tony, Romanoff]: string[]) => {
    //     console.log(peter);
    //     console.log(tony);
    //     console.log(Romanoff);
    // }
    // extraerArr(avengers);
    //////////// Desestructruación de Objetos y Arreglos
    ////////////  Promesas
    // // FUNCIONA SÓLO EN ES6
    // // {
    // // "compilerOptions": {
    // // /* Visit https://aka.ms/tsconfig.json to read more about this file */
    // // "target": "es5", CAMBIAR POR "es6"
    // // }
    // // }
    // console.log('Inicio');

    // const prom1 = new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         // resolve("terminó")
    //         reject("terminó");
    //     }, 1000);
    // })
    // prom1
    //     .then((mensaje) => console.log(mensaje))
    //     .catch((error) => console.warn(error));

    // // PROMESA CON TIPADO
    // const retirarDinero = (montoRetirar: number): Promise<number[]> => {
    //     let dineroActual = 10000;
    //     return new Promise((resolve, reject) => {
    //         setTimeout(() => {
    //             if (montoRetirar > dineroActual) {
    //                 reject(`No tiene fondos suficientes, cuenta con $${dineroActual.toLocaleString('de-DE')} pesos`)
    //             } else {
    //                 dineroActual -= montoRetirar;
    //                 resolve([montoRetirar, dineroActual])
    //             }
    //         }, 1000);
    //     })
    // }

    // // await retirarDinero(400)
    // retirarDinero(400)
    //     .then((res) => console.log(`Usted ha retirado $${res[0].toLocaleString('de-DE')} y le queda $${res[1].toLocaleString('de-DE')}.`))
    //     .catch(console.warn)

    // console.log('Fin');
    ////////////  Promesas
    //////////// Interfaces
    // // Reglas para el tipado de datos, así se evitan errores
    // // y tenemos un mayor control del codigo
    // interface Xmen {
    //     nombre: string;
    //     edad: number;
    //     poder?: string;
    // }

    // const enviarMision = (xmen: Xmen)=>{
    //     console.log(`Enviando a ${xmen.nombre}`);
    // }

    // const regresarCuartel = (xmen: Xmen)=>{
    //     console.log(`${xmen.nombre} ha regresado al cuartel`);
    // }

    // const wolverine: Xmen = {
    //     nombre: 'Wolverine',
    //     edad: 50,
    // }

    // enviarMision(wolverine);
    // regresarCuartel(wolverine);
    //////////// Interfaces
})();
